package ch.ti8m.docker.welcome;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.logging.Logger;

/**
 * Created by thomas on 14.12.15.
 */
@SpringBootApplication
@EnableAspectJAutoProxy
public class WelcomeApplication implements CommandLineRunner {

    private static Logger logger = Logger.getLogger(WelcomeApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(WelcomeApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        logger.info("Welcome Service started...");
    }
}

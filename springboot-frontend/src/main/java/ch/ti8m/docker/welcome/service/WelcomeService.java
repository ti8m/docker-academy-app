package ch.ti8m.docker.welcome.service;

/**
 * Created by thomas on 14.12.15.
 */
public interface WelcomeService {

    String welcome(String user);

}

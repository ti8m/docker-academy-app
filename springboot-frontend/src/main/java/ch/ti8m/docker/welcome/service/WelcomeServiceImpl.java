package ch.ti8m.docker.welcome.service;

import ch.ti8m.docker.welcome.api.WelcomeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by thomas on 14.12.15.
 */
@Service
@Primary
public class WelcomeServiceImpl implements WelcomeService {

    private static Logger logger = Logger.getLogger(WelcomeServiceImpl.class.getName());

    @Autowired
    private Environment environment;

    @Override
    public String welcome(String user) {
        String endpointUrl = getEndpointUrl(user);
        logger.info("calling welcome service url: " + endpointUrl);
        RestTemplate restTemplate = new RestTemplate();

        try {
            WelcomeModel welcomeModel = restTemplate.getForObject(endpointUrl, WelcomeModel.class);
            return welcomeModel.getMessage();
        } catch (RestClientException e) {
            logger.log(Level.SEVERE, "welcome service " + endpointUrl + " not available!");
            return "<welcome service not available>";
        }
    }

    private String getEndpointUrl(String user) {
        String welcomeSvcUrl = environment.getProperty("WELCOME_SVC_URL", "http://localhost:8081/welcome");
        final String endpointUrl = String.format("%s?name=%s", welcomeSvcUrl, user);
        logger.info("Calling welcome service " + endpointUrl);
        return endpointUrl;
    }
}

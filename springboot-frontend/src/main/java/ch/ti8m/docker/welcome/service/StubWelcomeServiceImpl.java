package ch.ti8m.docker.welcome.service;

import org.springframework.stereotype.Service;

/**
 * Created by thomas on 14.12.15.
 */
@Service
public class StubWelcomeServiceImpl implements WelcomeService {

    @Override
    public String welcome(String user) {
        return "Welcome " + user + " from Stub welcome service";
    }
}
